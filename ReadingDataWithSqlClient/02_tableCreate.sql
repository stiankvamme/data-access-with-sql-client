CREATE TABLE SuperheroesDb.dbo.Superhero(
	Id INT not null IDENTITY(1,1) PRIMARY KEY,
	Name nvarchar(255) not null,
	Alias nvarchar(255) not null,
	Origin nvarchar(255),
	--Power nvarchar(255) not null
);
--ALTER TABLE SuperHeroesDb.dbo.Superhero
	--  ADD Origin nvarchar(255) 
CREATE TABLE SuperheroesDb.dbo.Assistant(
	Id INT not null IDENTITY(1,1) PRIMARY KEY,
	Name nvarchar(255) not null
);
CREATE TABLE SuperheroesDb.dbo.Power(
	Id INT not null IDENTITY(1,1) PRIMARY KEY,
	Description nvarchar(255) not null,
	Name nvarchar(255) not null
);

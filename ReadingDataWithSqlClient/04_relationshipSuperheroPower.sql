CREATE TABLE SuperheroesDb.dbo.SuperheroPower (
        Superhero_Id int not null,
        Power_Id int not null,
        PRIMARY KEY (Superhero_Id, Power_Id),
		FOREIGN KEY (Superhero_Id) REFERENCES SuperHeroesDb.dbo.SuperHero (Id),
		FOREIGN KEY (Power_Id) REFERENCES SuperHeroesDb.dbo.Power (Id),
);

﻿using ReadingDataWithSqlClient.DataAccess;
using ReadingDataWithSqlClient.Models;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;

namespace ReadingDataWithSqlClient
{
    class Program
    {
        static void Main(string[] args)
        {
            CustomerRepository cr = new CustomerRepository();
            CustomerCountryRepository customerCountryRepo = new CustomerCountryRepository();
            CustomerSpenderRepository customerSpender = new CustomerSpenderRepository();
            CustomerGenreRepository customerGenreRepo = new CustomerGenreRepository();
            List<Customer> customers = (List<Customer>)cr.GetCustomers();//gets all customers in the database
            Customer c = cr.GetCustomerByID(34);//gets single customer from database with ID
            List<Customer> customerByName = (List<Customer>)cr.GetCustomerByName("Ole", "Normann"); //gets all customers with the same name from database
            List<Customer> customerLimited = (List<Customer>)cr.GetCustomerLimited(10, 5);// gets customers by ammount 10 and offset 5
            bool ok = cr.InsertCustomer(new Customer(-1, "jan", "Banan", "Norge", "5555", "40404040", "test@email.com"));// inserts one customer to DB
            bool upd = cr.UpdateCustomer(new Customer(63, "Karianne", "Nordmann", "Norge", "5555", "30303030", "test@email.com")); // updates single customer in database takes customer object
            bool del = cr.DeleteCustomer(67);//deletes single customer by id from database
            List<CustomerCountry> lccr = (List<CustomerCountry>)customerCountryRepo.GetCustomersPerCountry();//gets number of customers from  each country 
            List<CustomerSpender> cs = (List<CustomerSpender>)customerSpender.GetCustomersSpender();//gets customer and how much they have spent in decending order
            List<CustomerGenre> customerGenre = (List<CustomerGenre>)customerGenreRepo.GetCustomersFavoriteGenre(12);//gets a customers gets the genre customer have spent the most on, multiple if same amount 
        }
    }
}

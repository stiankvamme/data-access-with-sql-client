﻿using Microsoft.Data.SqlClient;
using ReadingDataWithSqlClient.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ReadingDataWithSqlClient.DataAccess
{
    public class CustomerCountryRepository : ICustomerCountry
    {
        /// <summary>
        /// gets all countries and numberof customers from that country, and return a list of Customers per country objects
        /// </summary>
        /// <returns>returns CustomerCountry object</returns>
        public IEnumerable<CustomerCountry> GetCustomersPerCountry()
        {
            List<CustomerCountry> customerCountry = new List<CustomerCountry>();
            try
            {
                SqlConnectionStringBuilder builder = new SqlConnectionStringBuilder();
                builder.DataSource = "ND-5CG0257SQQ\\SQLEXPRESS";
                builder.InitialCatalog = "Chinook";
                builder.IntegratedSecurity = true;
                using (SqlConnection connection = new SqlConnection(builder.ConnectionString))
                {
                    connection.Open();
                    string sql = "SELECT Country, Count( *) AS 'Number of Customers' FROM Chinook.dbo.Customer GROUP BY Country ORDER BY Count( *) DESC";
                    using (SqlCommand command = new SqlCommand(sql, connection))
                    {
                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                string country = reader.GetString(0);
                                int number = reader.GetInt32(1);
                                customerCountry.Add(new CustomerCountry(country, number));
                            }
                        }
                    }
                    connection.Close();
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
            return customerCountry;
        }
    }
}

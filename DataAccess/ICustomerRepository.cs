﻿using ReadingDataWithSqlClient.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ReadingDataWithSqlClient.DataAccess
{
    public interface ICustomerRepository
    {
        IEnumerable<Customer> GetCustomers();
        Customer GetCustomerByID(int customerId);
        bool InsertCustomer(Customer customer);
        bool DeleteCustomer(int customer);
        bool UpdateCustomer(Customer customer);
        IEnumerable<Customer> GetCustomerByName(string Firstname, string LastName);
    }
}

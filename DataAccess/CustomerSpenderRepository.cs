﻿using Microsoft.Data.SqlClient;
using ReadingDataWithSqlClient.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ReadingDataWithSqlClient.DataAccess
{
    class CustomerSpenderRepository : ICustomerSpender
    {
        /// <summary>
        /// gets the bigest spending customers in invoice table and order them in decending order
        /// </summary>
        /// <returns>Ienumerable customerSpender object</returns>
        public IEnumerable<CustomerSpender> GetCustomersSpender()
        {
            List<CustomerSpender> customerSpender = new List<CustomerSpender>();
            try
            {
                SqlConnectionStringBuilder builder = new SqlConnectionStringBuilder();
                builder.DataSource = "ND-5CG0257SQQ\\SQLEXPRESS";
                builder.InitialCatalog = "Chinook";
                builder.IntegratedSecurity = true;
                using (SqlConnection connection = new SqlConnection(builder.ConnectionString))
                {
                    connection.Open();
                    string sql = "SELECT MAX(Total) AS TotalSpent, Customer.FirstName, Customer.LastName FROM dbo.Invoice " +
                        "INNER JOIN Customer " +
                        "ON Customer.CustomerId = Invoice.CustomerId " +
                        "GROUP BY  Customer.FirstName, Customer.LastName " +
                        "ORDER BY MAX(Total) DESC";
                    using (SqlCommand command = new SqlCommand(sql, connection))
                    {
                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                decimal total = reader.GetDecimal(0);
                                string fname = reader.GetString(1);
                                string lname = reader.GetString(2);
                                customerSpender.Add(new CustomerSpender(total, fname, lname));
                            }
                        }
                    }
                    connection.Close();
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
            return customerSpender;
        }
    }
}

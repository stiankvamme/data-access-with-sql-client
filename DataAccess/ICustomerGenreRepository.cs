﻿using ReadingDataWithSqlClient.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ReadingDataWithSqlClient.DataAccess
{
    interface ICustomerGenreRepository
    {
        IEnumerable<CustomerGenre> GetCustomersFavoriteGenre(int customerId);
    }
}

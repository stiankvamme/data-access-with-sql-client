﻿using Microsoft.Data.SqlClient;
using ReadingDataWithSqlClient.Models;
using System;
using System.Collections.Generic;

namespace ReadingDataWithSqlClient.DataAccess
{
    class CustomerRepository : ICustomerRepository
    {
        /// <summary>
        /// gets all customers in the database even if som of the customer values are null
        /// </summary>
        /// <returns>IEnumerable object of the type Customer</returns>
        public IEnumerable<Customer> GetCustomers()
        {
            List<Customer> c = new List<Customer>();
            try
            {
                SqlConnectionStringBuilder builder = new SqlConnectionStringBuilder(); //connection string
                builder.DataSource = "ND-5CG0257SQQ\\SQLEXPRESS";
                builder.InitialCatalog = "Chinook";
                builder.IntegratedSecurity = true;
                using (SqlConnection connection = new SqlConnection(builder.ConnectionString))
                {
                    connection.Open();
                    string sql = "SELECT CustomerId, FirstName, LastName, ISNULL(Country, ''), ISNULL(PostalCode,''), ISNULL(Phone,''), ISNULL(Email,'') FROM dbo.Customer";
                    using (SqlCommand command = new SqlCommand(sql, connection))
                    {
                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                int id = reader.GetInt32(0);
                                string fname = reader.GetString(1);
                                string lname = reader.GetString(2);
                                string country = reader.GetString(3);
                                string pcode = reader.GetString(4);
                                string tlf = reader.GetString(5);
                                string email = reader.GetString(6);
                                Customer customer = new Customer(id, fname, lname, country, pcode, tlf, email);
                                c.Add(customer);
                            }
                        }
                    }
                    connection.Close();
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
            return c;
        }
        /// <summary>
        /// gets a single customer by id
        /// </summary>
        /// <param name="customerId"></param>
        /// <returns>Single customer object</returns>
        public Customer GetCustomerByID(int customerId)
        {
            Customer c = new Customer();
            try
            {
                SqlConnectionStringBuilder builder = new SqlConnectionStringBuilder();
                builder.DataSource = "ND-5CG0257SQQ\\SQLEXPRESS";
                builder.InitialCatalog = "Chinook";
                builder.IntegratedSecurity = true;

                string sql = "SELECT CustomerId, FirstName, LastName, ISNULL(Country, ''), ISNULL(PostalCode,''), ISNULL(Phone,''), ISNULL(Email,'') FROM Chinook.dbo.Customer WHERE CustomerId = @CustomerID";
                using (SqlConnection connection = new SqlConnection(builder.ConnectionString))
                {
                    connection.Open();
                    using (SqlCommand command = new SqlCommand(sql, connection))
                    {
                        command.Parameters.AddWithValue("@CustomerID", customerId);
                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {

                                c.Id = reader.GetInt32(0);
                                c.FirstName = reader.GetString(1);
                                c.LastName = reader.GetString(2);
                                c.Country = reader.GetString(3);
                                c.PostalCode = reader.GetString(4);
                                c.PhoneNumber = reader.GetString(5);
                                c.Email = reader.GetString(6);
                            }
                        }
                    }
                    connection.Close();
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
            return c;
        }
        /// <summary>
        /// gets customer from database with First name
        /// </summary>
        /// <param name="name"></param>
        /// <returns>, returns single Customer object</returns>
        public IEnumerable<Customer> GetCustomerByName(string fname, string lname)
        {
            List<Customer> customers = new List<Customer>();
            try
            {
                SqlConnectionStringBuilder builder = new SqlConnectionStringBuilder();
                builder.DataSource = "ND-5CG0257SQQ\\SQLEXPRESS";
                builder.InitialCatalog = "Chinook";
                builder.IntegratedSecurity = true;

                string sql = "SELECT CustomerId, FirstName, LastName, ISNULL(Country, ''), ISNULL(PostalCode,''), ISNULL(Phone,''), ISNULL(Email,'') FROM Chinook.dbo.Customer WHERE FirstName = @FirstName AND LastName=@LastName";
                using (SqlConnection connection = new SqlConnection(builder.ConnectionString))
                {
                    connection.Open();
                    using (SqlCommand command = new SqlCommand(sql, connection))
                    {
                        command.Parameters.AddWithValue("@FirstName", fname);
                        command.Parameters.AddWithValue("@LastName", lname);
                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                Customer c = new Customer();
                                c.Id = reader.GetInt32(0);
                                c.FirstName = reader.GetString(1);
                                c.LastName = reader.GetString(2);
                                c.Country = reader.GetString(3);
                                c.PostalCode = reader.GetString(4);
                                c.PhoneNumber = reader.GetString(5);
                                c.Email = reader.GetString(6);
                                customers.Add(c);
                            }
                        }
                    }
                    connection.Close();
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
            return customers;
        }
        /// <summary>
        /// gets an amount of customers where input selects which row to start getting customers from an how many
        /// </summary>
        /// <param name="limit"></param>
        /// <param name="offset"></param>
        /// <returns>IEnumerable customers object</returns>
        public IEnumerable<Customer> GetCustomerLimited(int limit, int offset)
        {
            List<Customer> c = new List<Customer>();
            try
            {
                SqlConnectionStringBuilder builder = new SqlConnectionStringBuilder();
                builder.DataSource = "ND-5CG0257SQQ\\SQLEXPRESS";
                builder.InitialCatalog = "Chinook";
                builder.IntegratedSecurity = true;
                using (SqlConnection connection = new SqlConnection(builder.ConnectionString))
                {
                    connection.Open();
                    string sql = "SELECT CustomerId, FirstName, LastName, ISNULL(Country, ''), ISNULL(PostalCode,''), ISNULL(Phone,''), ISNULL(Email,'') FROM Chinook.dbo.Customer " +
                                 "ORDER BY CustomerId OFFSET @offset ROWS FETCH NEXT @limit ROWS ONLY";
                    using (SqlCommand command = new SqlCommand(sql, connection))
                    {
                        command.Parameters.AddWithValue("@limit", limit);
                        command.Parameters.AddWithValue("@offset", offset);
                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                int id = reader.GetInt32(0);
                                string fname = reader.GetString(1);
                                string lname = reader.GetString(2);
                                string country = reader.GetString(3);
                                string pcode = reader.GetString(4);
                                string tlf = reader.GetString(5);
                                string email = reader.GetString(6);
                                Customer customer = new Customer(id, fname, lname, country, pcode, tlf, email);
                                c.Add(customer);
                            }
                        }
                    }
                    connection.Close();
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
            return c;
        }
        /// <summary>
        /// inserts new customer to DB takes in single customer object
        /// </summary>
        /// <param name="customer"></param>
        /// <returns>boolean if insert if insert=true  !insert=false</returns>
        public bool InsertCustomer(Customer customer)
        {
            bool inserted = false;
            string sql = "INSERT INTO Chinook.dbo.Customer(FirstName, LastName, Country, PostalCode, Phone, Email)" +
                "VALUES(@FirstName, @LastName, @Country , @PostalCode, @Phone, @Email)";
            try
            {
                SqlConnectionStringBuilder builder = new SqlConnectionStringBuilder();
                builder.DataSource = "ND-5CG0257SQQ\\SQLEXPRESS";
                builder.InitialCatalog = "Chinook";
                builder.IntegratedSecurity = true;
                using (SqlConnection connection = new SqlConnection(builder.ConnectionString))
                {

                    connection.Open();
                    using (SqlCommand command = new SqlCommand(sql, connection))
                    {
                        command.Parameters.AddWithValue("@CustomerId", customer.Id);
                        command.Parameters.AddWithValue("@FirstName", customer.FirstName);
                        command.Parameters.AddWithValue("@LastName", customer.LastName);
                        command.Parameters.AddWithValue("@Country", customer.Country);
                        command.Parameters.AddWithValue("@PostalCode", customer.PostalCode);
                        command.Parameters.AddWithValue("@Phone", customer.PhoneNumber);
                        command.Parameters.AddWithValue("@Email", customer.Email);
                        inserted = command.ExecuteNonQuery() > 0 ? true : false;
                    }
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
            return inserted;
        }
        /// <summary>
        /// updates customer takes inn single customer object
        /// </summary>
        /// <param name="customer"></param>
        /// <returns>boolean if update if update=true  !update=false</returns>
        public bool UpdateCustomer(Customer customer)
        {
            bool updated = false;
            string sql = "UPDATE Chinook.dbo.Customer SET FirstName = @Firstname, LastName= @Lastname, Country = @Country, Postalcode = @PostalCode, Phone = @Phone, Email=@Email " +
                " WHERE CustomerId = @CustomerId";
            try
            {
                SqlConnectionStringBuilder builder = new SqlConnectionStringBuilder();
                builder.DataSource = "ND-5CG0257SQQ\\SQLEXPRESS";
                builder.InitialCatalog = "Chinook";
                builder.IntegratedSecurity = true;
                using (SqlConnection connection = new SqlConnection(builder.ConnectionString))
                {

                    connection.Open();
                    using (SqlCommand command = new SqlCommand(sql, connection))
                    {
                        command.Parameters.AddWithValue("@CustomerId", customer.Id);
                        command.Parameters.AddWithValue("@FirstName", customer.FirstName);
                        command.Parameters.AddWithValue("@LastName", customer.LastName);
                        command.Parameters.AddWithValue("@Country", customer.Country);
                        command.Parameters.AddWithValue("@PostalCode", customer.PostalCode);
                        command.Parameters.AddWithValue("@Phone", customer.PhoneNumber);
                        command.Parameters.AddWithValue("@Email", customer.Email);
                        updated = command.ExecuteNonQuery() > 0 ? true : false;
                    }
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
            return updated;
        }
        /// <summary>
        /// deletes customer from DB takes in customer id
        /// </summary>
        /// <param name="customerId"></param>
        /// <returns>boolean if delete if deleted=true  !deleted=false</returns>
        public bool DeleteCustomer(int customerId)
        {
            bool deleted = false;

            try
            {
                SqlConnectionStringBuilder builder = new SqlConnectionStringBuilder();
                builder.DataSource = "ND-5CG0257SQQ\\SQLEXPRESS";
                builder.InitialCatalog = "Chinook";
                builder.IntegratedSecurity = true;
                using (SqlConnection conn = new SqlConnection(builder.ConnectionString))
                {
                    string sql = "DELETE FROM Chinook.dbo.Customer WHERE CustomerId=@CustomerId";
                    conn.Open();
                    using (SqlCommand command = new SqlCommand(sql, conn))
                    {
                        command.Parameters.AddWithValue("@CustomerID", customerId);
                        deleted = command.ExecuteNonQuery() > 0 ? true : false;
                    }
                }
            }
            catch (SqlException e)
            {
                Console.WriteLine(e.Message);
            }
            return deleted;
        }
    }
}

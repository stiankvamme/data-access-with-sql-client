﻿using Microsoft.Data.SqlClient;
using ReadingDataWithSqlClient.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ReadingDataWithSqlClient.DataAccess
{
    public class CustomerGenreRepository : ICustomerGenreRepository
    {
        /// <summary>
        /// gets a customer by first and last name and then gets the genre that customer has used the most money on
        /// </summary>
        /// <param name="c"></param>
        /// <returns>Ienumerable of CustomerGenre object</returns>
        public IEnumerable<CustomerGenre> GetCustomersFavoriteGenre(int id)
        {
            List<CustomerGenre> customerGenre = new List<CustomerGenre>();
            try
            {
                SqlConnectionStringBuilder builder = new SqlConnectionStringBuilder();
                builder.DataSource = "ND-5CG0257SQQ\\SQLEXPRESS";
                builder.InitialCatalog = "Chinook";
                builder.IntegratedSecurity = true;
                using (SqlConnection connection = new SqlConnection(builder.ConnectionString))
                {
                    connection.Open();
                    string sql = "SELECT TOP 1 WITH TIES g.Name AS FavoriteGenre, FirstName, LastName, SUM(il.Quantity * il.UnitPrice) AS AmountSpent FROM Invoice i JOIN Customer c ON i.CustomerId = c.CustomerId JOIN InvoiceLine il ON il.Invoiceid = i.InvoiceId JOIN Track t ON t.TrackId = il.Trackid JOIN Genre g ON t.GenreId = g.GenreId WHERE c.CustomerId=@CustomerId GROUP BY  c.FirstName, g.Name, FirstName, LastName ORDER BY AmountSpent DESC";
                    using (SqlCommand command = new SqlCommand(sql, connection))
                    {
                        command.Parameters.AddWithValue("@CustomerId", id);
                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                string genre = reader.GetString(0);
                                string fname = reader.GetString(1);
                                string lname = reader.GetString(2);
                                decimal amount = reader.GetDecimal(3);
                                customerGenre.Add(new CustomerGenre(genre, fname, lname, amount));
                            }
                        }
                    }
                    connection.Close();
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
            return customerGenre;
        }
    }
}

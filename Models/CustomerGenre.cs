﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ReadingDataWithSqlClient.Models
{
    public class CustomerGenre
    {
        public string Genre { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public decimal Amount { get; set; }
        public CustomerGenre(string genre, string fname, string lname, decimal amount)
        {
            this.Genre = genre;
            this.FirstName = fname;
            this.LastName = lname;
            this.Amount = amount;
        }
        /// <summary>
        /// makes string from stringbuilder to return a formated string from ToString for CustomerCountry
        /// </summary>
        /// <returns> string </returns>
        public override string ToString()
        {
            StringBuilder sb = new StringBuilder();
            sb.AppendLine("Genre | FirstName Lastname | Amount");
            sb.AppendLine(this.Genre + " | " + this.FirstName + " " + this.LastName + " | " + this.Amount);
            return sb.ToString();
        }

    }
}

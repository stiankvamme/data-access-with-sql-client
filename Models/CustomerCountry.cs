﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ReadingDataWithSqlClient.Models
{
    public class CustomerCountry
    {
        public string Country { get; set; }
        public int Number { get; set; }

        public CustomerCountry(string country, int number)
        {
            this.Country = country;
            this.Number = number;
        }
        /// <summary>
        /// makes string from stringbuilder to return a formated string from ToString for CustomerCountry
        /// </summary>
        /// <returns> string </returns>
        public override string ToString()
        {
            StringBuilder sb = new StringBuilder();
            sb.AppendLine("Country | Number");
            sb.AppendLine(this.Country + " | " + this.Number);
            return sb.ToString();
        }
    }
}

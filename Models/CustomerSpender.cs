﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ReadingDataWithSqlClient.Models
{
    public class CustomerSpender
    {
        public decimal Amount { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }

        public CustomerSpender(decimal amount, string fname, string lname)
        {
            this.Amount = amount;
            this.FirstName = fname;
            this.LastName = lname;
        }
        /// <summary>
        /// makes string from stringbuilder to return a formated string from ToString for CustomerSpender
        /// </summary>
        /// <returns> string </returns>
        public override string ToString()
        {
            StringBuilder sb = new StringBuilder();
            sb.AppendLine("Amount | FirstName | LastName");
            sb.AppendLine(this.Amount + "|" + this.FirstName + "|" + this.LastName);
            return sb.ToString();
        }
    }
}
